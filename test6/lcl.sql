-- 创建数据库
CREATE DATABASE sales_system;

-- 进入数据库
USE sales_system;

-- 创建用户表
CREATE TABLE user_table (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

-- 创建商品表
CREATE TABLE product_table (
    product_id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

-- 创建订单表
CREATE TABLE order_table (
    order_id INT PRIMARY KEY AUTO_INCREMENT,
    quantity INT NOT NULL,
    product_id INT NOT NULL,
    user_id INT NOT NULL,
    status VARCHAR(50) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id) REFERENCES product_table(product_id),
    FOREIGN KEY (user_id) REFERENCES user_table(user_id)
) ENGINE=InnoDB;

-- 插入测试数据
INSERT INTO user_table (username, password) VALUES ('admin', 'admin');
INSERT INTO user_table (username, password) VALUES ('test', 'test');

INSERT INTO product_table (name, price) VALUES ('手机', 3999.00);
INSERT INTO product_table (name, price) VALUES ('电视', 5999.00);

-- 创建备份表
CREATE TABLE user_backup (
    user_id INT PRIMARY KEY,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    backup_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

CREATE TABLE product_backup (
    product_id INT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    price DECIMAL(10, 2) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    backup_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB;

CREATE TABLE order_backup (
    order_id INT PRIMARY KEY,
    quantity INT NOT NULL,
    product_id INT NOT NULL,
    user_id INT NOT NULL,
    status VARCHAR(50) NOT NULL,
    create_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    update_time TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    backup_time TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    FOREIGN KEY (product_id) REFERENCES product_backup(product_id),
    FOREIGN KEY (user_id) REFERENCES user_backup(user_id)
) ENGINE=InnoDB;

-- 将数据备份到备份表中
INSERT INTO user_backup (user_id, username, password, create_time, backup_time) SELECT user_id, username, password, create_time, NOW() FROM user_table;
INSERT INTO product_backup (product_id, name, price, create_time, backup_time) SELECT product_id, name, price, create_time, NOW() FROM product_table;
INSERT INTO order_backup (order_id, quantity, product_id, user_id, status, create_time, update_time, backup_time) SELECT order_id, quantity, product_id, user_id, status, create_time, update_time, NOW() FROM order_table;

-- 删除数据表
DROP TABLE user_table;
DROP TABLE product_table;
DROP TABLE order_table;

-- 将备份数据恢复到数据表中
RENAME TABLE user_table TO user_table_old;
RENAME TABLE product_table TO product_table_old;
RENAME TABLE order_table TO order_table_old;

RENAME TABLE user_backup TO user_table;
RENAME TABLE product_backup TO product_table;
RENAME TABLE order_backup TO order_table;

-- 删除备份表
DROP TABLE user_table_old;
DROP TABLE product_table_old;
DROP TABLE order_table_old;