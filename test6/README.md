﻿<!-- markdownlint-disable MD033-->
<!-- 禁止MD033类型的警告 https://www.npmjs.com/package/markdownlint -->

# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 | [返回](./README.md)

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

## 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

## 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|
## 表结构详细设计

#### 用户表（user_table）

```sql
CREATE TABLE user_table (
  user_id NUMBER(10) PRIMARY KEY,
  username VARCHAR2(50),
  password VARCHAR2(50),
  create_time DATE
)
TABLESPACE common_data;
```

说明：

- user_id：用户ID。
- username：用户名。
- password：密码。
- create_time：创建时间。

#### 商品表（product_table）

```sql
CREATE TABLE product_table (
  product_id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(50),
  price NUMBER(10, 2),
  create_time DATE
)
TABLESPACE sales_data;
```

说明：

- product_id：商品ID。
- name：商品名称。
- price：商品价格。
- create_time：创建时间。

#### 订单表（order_table）

```sql
CREATE TABLE order_table (
  order_id NUMBER(10) PRIMARY KEY,
  quantity NUMBER(10),
  product_id NUMBER(10),
  user_id NUMBER(10),
  status VARCHAR2(50),
  create_time DATE,
  update_time DATE,
  CONSTRAINT fk_product FOREIGN KEY (product_id) REFERENCES product_table (product_id),
  CONSTRAINT fk_user FOREIGN KEY (user_id) REFERENCES user_table (user_id)
)
TABLESPACE sales_data;
```

说明：

- order_id：订单ID。
- quantity：购买数量。
- product_id：商品ID，外键引用商品表。
- user_id：用户ID，外键引用用户表。
- status：订单状态。
- create_time：创建时间。
- update_time：更新时间。

#### 配置表（config_table）

```sql
CREATE TABLE config_table (
  config_id NUMBER(10) PRIMARY KEY,
  name VARCHAR2(50),
  value VARCHAR2(50),
  create_time DATE
)
TABLESPACE common_data;
```

说明：

- config_id：配置ID。
- name：配置名称。
- value：配置值。
- create_time：创建时间。

## 用户及权限分配方案

我们将创建两个用户：`sales_admin`和`sales_user`。前者拥有完全的数据库权限，后者只能查询和插入数据。创建方式如下：

```sql
-- 创建管理员用户
CREATE USER sales_admin IDENTIFIED BY admin_password;

-- 赋予管理员权限
GRANT ALL PRIVILEGES TO sales_admin;

-- 创建普通用户
CREATE USER sales_user IDENTIFIED BY user_password;

-- 赋予查询和插入权限
GRANT SELECT, INSERT ON user_table TO sales_user;
GRANT SELECT, INSERT ON order_table TO sales_user;
GRANT SELECT ON product_table TO sales_user;
```

请注意，为了加强数据库安全性，在实际应用中，我们应该对这些用户的密码进行加密。此外，我们也可以根据具体业务需要，为不同用户分配不同的权限。

## 程序包设计

```sql
CREATE OR REPLACE PACKAGE sales_pkg IS
  function verify_user(username IN VARCHAR2, password IN VARCHAR2) RETURN NUMBER;
  procedure create_order(user_id IN NUMBER, product_id IN NUMBER);
  procedure update_order(order_id IN NUMBER, status IN VARCHAR2);
END sales_pkg;
/

CREATE OR REPLACE PACKAGE BODY sales_pkg IS
  function verify_user(username IN VARCHAR2, password IN VARCHAR2) RETURN NUMBER IS
    user_id NUMBER(10);
  BEGIN
    SELECT user_id INTO user_id FROM user_table WHERE username = username AND password = password;
    RETURN user_id;
  EXCEPTION
    WHEN no_data_found THEN
      RETURN null;
  END;
  
  procedure create_order(user_id IN NUMBER, product_id IN NUMBER) IS
    order_id NUMBER(10);
  BEGIN
    SELECT order_seq.nextval INTO order_id FROM dual;
    INSERT INTO order_table (order_id, quantity, product_id, user_id, status, create_time, update_time)
    VALUES (order_id, 1, product_id, user_id, '新建', SYSDATE, SYSDATE);
  EXCEPTION
    WHEN OTHERS THEN
      raise_application_error(-20001, '订单创建失败');
  END;
  
  procedure update_order(order_id IN NUMBER, status IN VARCHAR2) IS
  BEGIN
    UPDATE order_table SET status = status, update_time = SYSDATE WHERE order_id = order_id;
  EXCEPTION
    WHEN no_data_found THEN
      raise_application_error(-20002, '订单不存在');
  END;
END sales_pkg;
/
```

# 备份方案设计

为了保证数据安全，我们将定期进行数据库备份。备份包括全备份和增量备份，同时将备份目录设置到独立磁盘上以防止原始数据和备份数据同时受到硬件故障的影响。备份的实现方式有多种，其中 RMAN 是 Oracle 数据库自带的备份工具，非常方便实用，我们采用 RMAN 来实现备份。

我们将设置每天执行一次全备份，并在此基础上进行增量备份。备份文件将存储到独立的磁盘上，并设置远程存储备份文件。具体实现步骤如下：

1. 创建存储备份文件的目录

```sql
CREATE DIRECTORY backup_dir AS '/backup';
```

2. 设定备份计划

```sql
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
    job_name => 'Sales_Backup',
    job_type => 'BACKUP_SCRIPT',
    job_action => 'BACKUP INCREMENTAL LEVEL 0 DATABASE TAG ''SALES_DAILY_BACKUP''',
    schedule_name => 'Sales_Backup_Daily_Schedule',
    enabled => TRUE
  );
END;
/
```

3. 创建日志表

```sql
CREATE TABLE backup_log (
  start_time TIMESTAMP,
  end_time TIMESTAMP,
  status VARCHAR2(20)
);
```

4. 设定备份日志监控

```sql
BEGIN
  DBMS_SCHEDULER.CREATE_JOB (
    job_name => 'Sales_Backup_Log_Monitor',
    job_type => 'PLSQL_BLOCK',
    job_action => 'BEGIN
                     DECLARE
                       last_end_time TIMESTAMP;
                     BEGIN
                       SELECT MAX(end_time) INTO last_end_time FROM backup_log WHERE status = ''SUCCESS'';
                       IF last_end_time IS NULL THEN
                         last_end_time := SYSTIMESTAMP - NUMTODSINTERVAL(1, ''DAY'');
                       END IF;
                       DBMS_SCHEDULER.DROP_JOB(''Sales_Backup_Monitor'');
                       DBMS_SCHEDULER.CREATE_JOB (
                         job_name => ''Sales_Backup_Monitor'',
                         job_type => ''BACKUP_LOG_MONITOR'',
                         repeat_interval => ''FREQ=MINUTELY;INTERVAL=5'',
                         job_action => ''CHECKPOINT_TIME='' || TO_CHAR(last_end_time, ''YYYY-MM-DD HH24:MI:SS''),
                         enabled => TRUE
                       );
                     END;
                   END;',
    enabled => TRUE
  );
END;
/
```

说明：这个备份日志监控定时器会每隔 5 分钟检查备份日志表中上次成功备份的时间，并根据该时间设置增量备份的起始时间。
