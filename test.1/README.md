# 3班，201910414315，刘城林
# 实验1：SQL语句的执行计划分析与优化指导

## 实验目的

  分析SQL执行计划，执行SQL语句的优化指导。理解分析SQL语句的执行计划的重要作用。

## 实验数据库和用户

  数据库是pdborcl，用户是sys和hr

## 实验内容

- 对Oracle12c中的HR人力资源管理系统中的表进行查询与分析。
- 设计自己的查询语句，并作相应的分析，查询语句不能太简单。执行两个比较复杂的返回相同查询结果数据集的SQL语句，通过分析SQL语句各自的执行计划，判断哪个SQL语句是最优的。最后将你认为最优的SQL语句通过sqldeveloper的优化指导工具进行优化指导，看看该工具有没有给出优化建议。

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

- 用户hr默认没有统计权限，打开统计信息功能autotrace时要报错，必须要向用户hr授予以下视图的选择权限：

-- 查询员工姓名、薪水、所在部门名称及对应经理姓名
-- 对多个表进行连接和聚合查询
SELECT e.last_name "员工姓名", e.salary "薪水", d.department_name "所在部门", m.last_name "部门经理"
FROM employees e
JOIN departments d ON e.department_id = d.department_id
JOIN employees m ON d.manager_id = m.employee_id
ORDER BY e.salary DESC;

-- 查询入职日期在2005年以后的员工在各个部门的平均薪水
-- 使用子查询和聚合查询
SELECT d.department_name "所在部门", AVG(e.salary) "平均薪水"
FROM employees e
JOIN departments d ON e.department_id = d.department_id
WHERE EXTRACT(YEAR FROM e.hire_date) >= 2005
GROUP BY d.department_name;

## 查询一
$ sqlplus hr/@localhost/pdborcl

-- 打开统计信息功能 autotrace
SET AUTOTRACE ON

-- 查询部门人数和平均薪资
SELECT d.department_name, COUNT(e.job_id) AS "总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
AND d.department_name IN ('sys', 'hr')
GROUP BY d.department_name;

## 分析
该查询语句通过联合查询员工表（employees）和部门表（departments），来查询指定部门（sys 和 hr）的总人数和平均工资，并按照部门名称（d.department_name）进行分组查询。查询中使用了 count 和 avg 聚合函数来统计每个部门的总人数和平均工资。联合查询中使用了 where 子句和相等连接符将两个表连接起来，从而筛选出需要的数据。对于查询较大的表而言，这种连接可能会导致性能问题，因此，在优化查询语句的过程中，需考虑增加索引或使用其他连接方式等方法来提高性能和减少资源消耗。在进行具体性能优化时，可以通过分析查询计划和访问指导工具来评估、优化查询语句。如果需要进一步优化查询性能，可以通过创建推荐的索引或修改物理方案等方式进行优化。最终目的是使查询语句能够更加高效地访问数据库，提升整个应用系统的响应速度和用户体验。



## 查询二
$ sqlplus hr/@localhost/pdborcl

-- 打开统计信息功能 autotrace
SET AUTOTRACE ON

-- 查询部门人数和平均薪资
SELECT d.department_name, COUNT(e.job_id) AS "总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d, hr.employees e
WHERE d.department_id = e.department_id
GROUP BY d.department_name
HAVING d.department_name IN ('sys', 'hr');

## 分析
查询二在执行效率和查询结果精准度上都优于查询一。因为在查询一中，由于使用了 where 子句进行部门和员工的筛选，可能会导致一些没有意义或不需要的数据出现在结果中。而查询二先进行 where 过滤，再在聚合时使用 having 子句，可以更细致地精选筛选结果，从而更加准确和高效地完成查询任务。

## 优化代码
$ sqlplus hr/@localhost/pdborcl

-- 打开统计信息功能 autotrace
SET AUTOTRACE ON

-- 查询部门人数和平均薪资
SELECT d.department_name, COUNT(e.job_id) AS "总人数", AVG(e.salary) AS "平均工资"
FROM hr.departments d
JOIN hr.employees e
ON e.department_id = d.department_id
WHERE d.department_name IN ('sys', 'hr')
GROUP BY d.department_name;``SQL
set autotrace on

SELECT d.department_name,count(e.job_id) as "部门总人数",avg(e.salary) as "平均工资"
FROM  hr.departments d,hr.employees e
WHERE e.department_id=d.department_id and d.department_id in 
(SELECT department_id from hr.departments WHERE department_name in ('sys','hr')) 
group by d.department_name;

## 分析
该查询语句对查询一进行了优化，并且在这个过程中运用了一些技巧。首先，它使用 join 子句来替换原本联合查询语句的表连接方式，从而提高查询效率。其次，它使用 where 子句来进行部门 ID 和部门名称之间关联的连接操作。特别的是使用 where 对子查询进行部门名称和部门 ID 的映射，然后再传入 join 子句，让它来完成部门 ID 和员工信息的筛选和匹配。这种方式可以更好的完成对所需数据的筛选并使查询结果更精准和更高效。最后，该查询语句使用了 group by 子句对查询结果按照部门名称进行分组并进行统计，进一步提高了查询效率。

同时，在对该查询语句进行优化时，我们还可以考虑进一步使用索引优化技术或者其他技术来提高查询效率，比如使用数据库分区技术、更好地设置索引策略、使用 CBO（Cost-based Optimization）方式等等。总之，通过不断地对查询语句进行优化，我们可以提升数据库的性能和效率，提高查询响应速度和用户体验。

