# 3班，201910414315，刘城林
# 实验2：用户及权限管理

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：

$ sqlplus system/123@pdborcl

-- 创建本地角色 con_res_role，授权 CONNECT、RESOURCE 和 CREATE VIEW 权限
CREATE ROLE con_res_role;
GRANT CONNECT, RESOURCE, CREATE VIEW TO con_res_role;

-- 创建用户 sale，指定默认表空间和临时表空间，并分配 50MB 的表空间限额
CREATE USER sale IDENTIFIED BY 123 DEFAULT TABLESPACE users TEMPORARY TABLESPACE temp;
ALTER USER sale DEFAULT TABLESPACE users;
ALTER USER sale QUOTA 50M ON users;

-- 授予 con_res_role 角色给用户 sale
GRANT con_res_role TO sale;

-- 测试 sale 用户是否能执行创建表、插入数据、创建视图以及查询数据操作
-- 创建表
CREATE TABLE mytable (id NUMBER, name VARCHAR2(50));
-- 插入数据
INSERT INTO mytable VALUES (1, 'Alice');
-- 创建视图
CREATE VIEW myview AS SELECT * FROM mytable WHERE id > 0;
-- 查询表
SELECT * FROM mytable;
-- 查询视图
SELECT * FROM myview;

-- 收回 con_res_role 角色
REVOKE con_res_role FROM sale;

$ sqlplus sale/123@pdborcl

-- 创建表 customers 和视图 customers_view，并插入数据
CREATE TABLE customers (id NUMBER, name VARCHAR2(50)) TABLESPACE users;
INSERT INTO customers (id, name) VALUES (1, 'zhang');
INSERT INTO customers (id, name) VALUES (2, 'wang');
CREATE VIEW customers_view AS SELECT name FROM customers;

-- 将 customers_view 视图的 SELECT 对象权限授予 hr 用户
GRANT SELECT ON customers_view TO hr;

-- 查询会话权限和角色
SELECT * FROM session_privs;
SELECT * FROM session_roles;

-- 查询 customers_view 视图的数据
SELECT * FROM customers_view;

$ sqlplus hr/123@pdborcl

-- 查询 sales 用户共享给 hr 用户的视图 customers_view 中的数据
SELECT * FROM sale.customers_view;

-- 尝试查询 sales 用户的表 customers，因为该表没有共享给 hr，所以会报错
SELECT * FROM sale.customers;
-- 报错：表或视图不存在

-- 创建共享表 employees，并在 sales 用户和 hr 用户之间测试读写共享和只读共享
-- 创建表 employees 并插入数据
CREATE TABLE employees (id NUMBER, name VARCHAR2(50)) TABLESPACE users;
INSERT INTO employees (id, name) VALUES (1, 'Alice');
INSERT INTO employees (id, name) VALUES (2, 'Bob');

-- 在 sales 用户和 hr 用户之间尝试读写共享
-- 在 sales 用户会话中插入新的数据
INSERT INTO employees (id, name) VALUES (3, 'Charlie');
-- 在 hr 用户会话中查询数据
SELECT * FROM sales.employees;
-- 在 hr 用户会话中插入新的数据
INSERT INTO employees (id, name) VALUES (4, 'Dave');
-- 在 sales 用户会话中查询数据
SELECT * FROM hr.employees;

-- 在 sales 用户和 hr 用户之间尝试只读共享
-- 在 hr 用户会话中查询数据
SELECT * FROM sales.employees;
-- 报错：表或视图不存在（因为没有共享 SELECT 权限）
-- 在 sales 用户会话中授权 hr 用户只读共享权限
GRANT SELECT ON employees TO hr;
-- 在 hr 用户会话中查询数据
SELECT * FROM sales.employees;

## $ sqlplus system/123@pdborcl

-- 将默认概要文件的 FAILED_LOGIN_ATTEMPTS 设置为 3
ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;

-- 测试用户登录时失败 3 次后被锁定
-- 第 1 次错误密码
sqlplus hr/wrong_password@pdborcl
-- 输出：ERROR:
-- ORA-01017: invalid username/password; logon denied

-- 第 2 次错误密码
sqlplus hr/wrong_password@pdborcl
-- 输出：ERROR:
-- ORA-01017: invalid username/password; logon denied

-- 第 3 次错误密码，由于用户被锁定，此时会直接输出错误信息
sqlplus hr/wrong_password@pdborcl
-- 输出：ERROR:
-- ORA-28000: the account is locked

-- 使用 system 用户登录解锁 hr 用户
ALTER USER hr ACCOUNT UNLOCK;

## 数据库和表空间占用分析

$ sqlplus system/123@pdborcl

-- 查询当前数据库所有表空间的磁盘使用量和空闲空间
SELECT tablespace_name, SUM(bytes) total_bytes, SUM(bytes - blocks * 8192) used_bytes, SUM(blocks * 8192) free_bytes
FROM dba_data_files
GROUP BY tablespace_name;

-- 查看默认表空间 users 的使用情况
SELECT tablespace_name, SUM(bytes) total_bytes, SUM(bytes - blocks * 8192) used_bytes, SUM(blocks * 8192) free_bytes
FROM dba_segments
WHERE tablespace_name = 'USERS'
GROUP BY tablespace_name;

-- 查看 sale 用户的表 customers 和视图 customers_view 占用的空间
SELECT owner, segment_name, segment_type, tablespace_name, SUM(bytes) total_bytes, SUM(bytes - blocks * 8192) used_bytes, SUM(blocks * 8192) free_bytes
FROM dba_segments
WHERE owner = 'SALE' AND segment_name IN ('CUSTOMERS', 'CUSTOMERS_VIEW')
GROUP BY owner, segment_name, segment_type, tablespace_name;

## 查看数据库的使用情况

$ sqlplus system/123@pdborcl

-- 查询 users 表空间的数据文件名、大小、最大容量以及是否自动增加等信息
SELECT tablespace_name, file_name, bytes/1024/1024 "MB", maxbytes/1024/1024 "MAX_MB", autoextensible
FROM dba_data_files
WHERE tablespace_name = 'USERS';

-- 查询各表空间的大小、剩余空间、使用空间和使用率等信息
SELECT a.tablespace_name "表空间名",
    Total/1024/1024 "大小MB",
    free/1024/1024 "剩余MB",
    (total - free)/1024/1024 "使用MB",
    ROUND((total - free)/total, 4)*100 "使用率%"
FROM (SELECT tablespace_name, SUM(bytes) free FROM dba_free_space GROUP BY tablespace_name) a,
    (SELECT tablespace_name, SUM(bytes) total FROM dba_data_files GROUP BY tablespace_name) b
WHERE a.tablespace_name = b.tablespace_name;

## 实验结束删除用户和角色
$ sqlplus system/123@pdborcl

-- 删除角色 con_res_role 和用户 sale
DROP ROLE con_res_role;
DROP USER sale CASCADE;


## 实验注意事项

- 完成时间：2023-04-25，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test2目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
