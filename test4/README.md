# 3班，201910414315，刘城林
# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## SET SERVEROUTPUT ON;

DECLARE
  TYPE t_number IS VARRAY(100) OF INTEGER NOT NULL; -- 数组
  i INTEGER;
  j INTEGER;
  spaces VARCHAR2(30) := '   '; -- 三个空格，用于打印时分隔数字
  N INTEGER := 9; -- 一共打印9行数字
  rowArray t_number := t_number();
BEGIN
  DBMS_OUTPUT.PUT_LINE('1'); -- 先打印第1行
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' ')); -- 先打印第2行
  DBMS_OUTPUT.PUT(RPAD(1, 9, ' ')); -- 打印第一个1
  DBMS_OUTPUT.PUT_LINE(''); -- 打印换行
  -- 初始化数组数据
  FOR i IN 1..N LOOP
    rowArray.EXTEND;
  END LOOP;
  rowArray(1) := 1;
  rowArray(2) := 1;    
  FOR i IN 3..N -- 打印每行，从第3行起
  LOOP
    rowArray(i) := 1;
    j := i - 1;
    --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
    --这里从第j-1个数字循环到第2个数字，顺序是从右到左
    WHILE j>1 LOOP
      rowArray(j) := rowArray(j) + rowArray(j-1);
      j := j - 1;
    END LOOP;
    --打印第i行
    FOR j IN 1..i LOOP
      DBMS_OUTPUT.PUT(RPAD(rowArray(j), 9, ' ')); --打印第一个1
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(''); --打印换行
  END LOOP;
END;
/
## 实验注意事项

- 完成时间：2023-05-15，请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的oracle项目中的test4目录中。
- 实验分析及结果文档说明书用Markdown格式编写。
